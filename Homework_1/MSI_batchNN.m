function [theta1_new,theta2_new,theta3_new, J, h, y, delta2, delta3, delta4] = MSI_batchNN(inputData,theta1,theta2,theta3,LearningRate)
temp_delta1 =0;
temp_delta2 =0;
temp_delta3 =0;
J =0;
for n=1:size(inputData,1)
    
    
    a1 = [1 inputData(n,1:2)]'; %input with bais value
    
    %% Forward propagation
    a2 = forwardProp(theta1,a1);
    %a3 = forwardProp(theta2,[1;a2]);
    a3 = theta2*[1;a2];
    a4 = theta3*[1;a3];
    %h = forwardProp(1,a3);
    h = activationFun(-a4);
    %% Back propagation
    % Calculate error at the output layer
    y = inputData(n,3); %expected output
    delta4 = h - y;
    % error in layer 3
    delta3t = ((theta3' * delta4) .* ([1;a3].*(1-[1;a3])));
    delta3 = delta3t(2:end);
    % error in layer 2
    delta2t = ((theta2' * delta3) .* ([1;a2].*(1-[1;a2])));
    delta2 = delta2t(2:end);
    
    % accumulate partial derivatives
    temp_delta3 = temp_delta3 + (delta4 * [1;a3]');
    temp_delta2 = temp_delta2 + (delta3 * [1;a2]');
    temp_delta1 = temp_delta1 + (delta2 * [a1]');
    
    J = J + costFun(y,h);
end
J = J/-n;
% adjust weights
theta3_new = theta3 - (LearningRate * (temp_delta3/n));
theta2_new = theta2 - (LearningRate * (temp_delta2/n));
theta1_new = theta1 - (LearningRate * (temp_delta1/n));

end

