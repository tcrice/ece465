function [t,l] = circleGen(n)
xdim = 28;
ydim = 28;
xc = 15;
yc = 15;
rad = 7;
data_r = zeros(xdim,ydim,3);
data_g= zeros(xdim,ydim,3);
[X,Y] = meshgrid(1:xdim,1:ydim);
data1 = zeros(xdim,ydim);
data2 = zeros(xdim,ydim);

%% Red Circle Generation
for n = 1:15
   for m = 1:numel(X)
   c1 =  (X(m)-xc)^2+(Y(m)-yc)^2;
   if (c1 <= rad^2)
      data1(m) = 1; 
   end
   end
   data_r(:,:,1) = data1;
   %red = data_r;
   r = data_r(:);
   t(n,:) = r;
   l(n,:) = 1;
end

for n = 16:30
   for m=1:numel(X)
   c2 = (X(m)-xc)^2+(Y(m)-yc)^2;
   if (c2 <= rad^2)
       data2(m) = 1;
   end
   end
   data_g(:,:,2) = data2;
   green = data_g;
    g = green(:);
    t(n,:) = g;
    l(n,:) = 0;
end
