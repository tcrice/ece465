% costFun - calculate cost function


function [J] = costFun(y,h)

%J = ((y * log(h)) + ((1 - y) * log(1 - h))) ;
J=(h-y)^2;
end