

% Dr. Mohammad Imtiaz
% ECE 465 Spring 2018
% Example: Making circles for neural network training
%% ================================================================================

%%
xdim = 28;
ydim = 28;
xc = 15;
yc = 15;
r = 7;
data1 = zeros(xdim,ydim,3);
data2 = zeros(xdim,ydim,3);

%%
[X,Y] = meshgrid(1:xdim,1:ydim);

for n=1:numel(X)
    
    
    circleData = (X(n)-xc)^2+(Y(n)-yc)^2;
    if (circleData<=r^2)
        data1(n) = 1;
        %data_r = (28,28,3);
        red = circleData(:);
        t(n) = red;
        l(n) = 1;
    end
end

figure
imagesc(data1)