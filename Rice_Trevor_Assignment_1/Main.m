% Main NN
%%
%{
First lets clear our workspace and close all windows.  
Note: generally this is not recommended unless you plan to run only your 
program in isolation.
%}
clear all, close all, clc;
%% Define hyperparameters for our network
LearningRate = 0.1;
noEpochs = 100001;
plotRate = 101; % How often to test and display the network performance
%%=========================================================================

%% Test Data set - self explanatory
inputDataAND = [0 0 0
                0 1 0
                1 0 0
                1 1 1];

inputDataXOR = [0 0 0
                0 1 1
                1 0 1
                1 1 0];

inputDataOR =  [0 0 0
                0 1 1
                1 0 1
                1 1 1];
            
% pick one data set for this network            
inputData = inputDataOR; % assign any one of the data from above
%%=========================================================================
%% weights or parameters
%{
    \begin{equation*}
\theta^1 = 
\begin{bmatrix}
\theta{^1}_{(1,1)} & \theta{^1}_{(1,2)} & \theta{^1}_{(1,3)} \\
\theta{^1}_{(2,1)} & \theta{^1}_{(2,2)} & \theta{^1}_{(2,3)} \\
\end{bmatrix}
\end{equation*}


Remember from lecture: $\theta{^l}_{(i,j)}$ means weight for $\mathit{l}$ connecting node $\mathit{j}$ in layer  $\mathit{l}$ to node  $\mathit{i}$ in layer $\mathit{l+1}$. 
%}
% Assign weights randomly at start but not zeros! why?
theta1 = 2*rand(2,3)-1; %weights layer 1 to 2
theta2 = 2*rand(1,3)-1; %weights layer 1 to 3
theta3 = 2*rand(1,3)-1; %weights layer 1 to 4
theta4 = 2*rand(1,3)-1; %weights layer 1 to 5


%%

xt = inputData(1,1:2)';
a1 = [1;xt]; %input with bais value

%% Forward propagation

a2 = forwardProp(theta1,a1);
a3 = forwardProp(theta2,[1;a2]);
a4 = forwardProp(theta3,[1;a3]);
a5 = forwardProp(theta4,[1;a4]);
h = forwardProp(1,a5);

%% Back propagation

% Calculate error at the output layer
y = inputData(1,3); %expected output
delta3 = h - y;
% error in layer 2
delta2t = ((theta2' * delta3) .* ([1;a2].*(1-[1;a2])));
delta2 = delta2t(2:end);
% adjust weights
theta2 = theta2 - (LearningRate * (delta3 * [1;a2]'));
theta1 = theta1 - (LearningRate * (delta2 * [a1]'));

[theta1_new,theta2_new, J, h, y, delta2, delta3] = MSI_batchNN(inputData,theta1,theta2,LearningRate);



%%=========================================================================

%% Start training the network
saveData = zeros(round(noEpochs/plotRate),4); %setup matrix to save data
k=1;
for n=1:noEpochs
    % run one epoch and update the weights 
    [theta1,theta2, J, h, y, delta2, delta3] = MSI_batchNN(inputData,theta1,theta2,LearningRate);
    
    % test the network ocassionally and save data
    if(mod(n,plotRate)==0)
        saveData(k,:) = [J delta2' delta3];
        k=k+1;
        %----------------------------------------------------------------------
        % testing
        disp(['Epoch no:' num2str(n)  ', J=' num2str(J)])
        [theta1,theta2, J, h, y, delta2, delta3] = MSI_batchNN([0 0 0],theta1,theta2,LearningRate);
        disp(['[0 0] ->' num2str(h)]);
        [theta1,theta2, J, h, y, delta2, delta3] = MSI_batchNN([0 1 0],theta1,theta2,LearningRate);
        disp(['[0 1] ->' num2str(h)]);
        [theta1,theta2, J, h, y, delta2, delta3] = MSI_batchNN([1 0 0],theta1,theta2,LearningRate);
        disp(['[1 0] ->' num2str(h)]);
        [theta1,theta2, J, h, y, delta2, delta3] = MSI_batchNN([1 1 1],theta1,theta2,LearningRate);
        disp(['[1 1] ->' num2str(h)]);
    end    
end
%% =========================================================================


