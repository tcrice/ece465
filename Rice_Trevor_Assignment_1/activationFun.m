% activationFun


function [z] = activationFun(x)

z = 1./(1+exp(x));

end